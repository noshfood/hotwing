'use strict';

var lb = require('loopback');
var boot = require('loopback-boot');
var colors = require('colors');
var _ = require('lodash');

global.loopback = module.exports = lb();

// loopback.log = require('../weaponry/logger');

loopback.start = () => {
	// start the web server
	var server = loopback.listen(() => {
		loopback.emit('started', server);

		console.log(require('../client/ascii'));
	});

	return server;
};

// var consoleTasks = require('../weaponry/task')(loopback);

boot(loopback, __dirname, (err) => {
	if (err) throw err;

	// if (consoleTasks.check()) {
	// 	loopback.taskRunning = true;
	// 	consoleTasks.run(false, false, true);
	// }
	// else if (require.main === module) {
		loopback.start();
	// }
});
